(function ($, window, document, undefined) {

	$(document).ready(function($) {

		// Timeline Slider
		$('.timeline-slider').slick({
			dots: false,
			arrows: false,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 9000,
			adaptiveHeight: true
		});

		// Go to Timeline Slider + Active Dot
		$('.timeline-link').on('click', function(e){
			var slideIndex = $(this).index();
			$('.timeline-slider').slick('slickGoTo', parseInt(slideIndex) );

			$('.timeline-link').removeClass('active');
			$(this).addClass('active');
		});

		// Active Dot on change
		$('.timeline-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
			var activeSlideIndex = currentSlide + 1;
			var active_link_query = '.timeline-link:nth-child(' + activeSlideIndex + ')';
			$('.timeline-link').removeClass('active');
			$(active_link_query).addClass('active');
		});
		
		$('.search-toggle svg').on('click', function(){
			let form = $(this).siblings('.search-form-wrapper');

			$(form).toggleClass('active');

			return false;
		});

	});

	$(document).mouseup(function(e) {
		var search_modal = $('.search-form-wrapper');
		if (!search_modal.is(e.target) && search_modal.has(e.target).length === 0) {
			$('.search-form-wrapper').removeClass('active');
		}
	});

	$(document).keyup(function(e) {		
		if (e.keyCode == 27) {
			$('.search-form-wrapper').removeClass('active');
		}
	});

})(jQuery, window, document);