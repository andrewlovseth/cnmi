<?php get_header(); ?>

    <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
        <section class="default-content grid">
            <div class="copy p1 extended">
		        <?php the_content(); ?>
            </div>
        </section>
    <?php endwhile; endif; ?>

	<?php get_template_part('template-parts/footer/feedback-news'); ?>


<?php get_footer(); ?>