<?php

// Pull data from Google Spredsheeet Source
function esa_get_sheet_data($transient_name, $spreadsheet_ID, $timeout, $sheet = NULL) {

    if($sheet !== NULL) {
        $url = "https://sheets.googleapis.com/v4/spreadsheets/" . $spreadsheet_ID . "/values/" . urlencode($sheet) . "!A2:Z?key=AIzaSyAubeoNNeqyDqelihJCEaK9qAVLMT_llwQ";
    } else {
        $url = "https://sheets.googleapis.com/v4/spreadsheets/" . $spreadsheet_ID . "/values/A2:Z?key=AIzaSyAubeoNNeqyDqelihJCEaK9qAVLMT_llwQ";
    }

    //var_dump($url);

    if(get_transient($transient_name)) {

        $transient = get_transient($transient_name);
        return $transient;

    } else {

        $response = wp_remote_get($url);
        $api_response = json_decode( wp_remote_retrieve_body( $response ), true );
        $values = $api_response['values'];

        if($values) {
            set_transient($transient_name, $values, $timeout);
            return $values;
        }        
    }
}