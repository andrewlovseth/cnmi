<?php

$search_term = get_search_query();
global $wp_query;
$count = $wp_query->post_count;

get_header(); ?>

	<section class="archived-posts search-results grid">

        <section class="page-header">
            <div class="headline">
                <h1 class="small">Search Results: <?php echo $search_term; ?></h1>
            </div>

            <?php if($count > 0): ?>
                <div class="results-count">
                    <p><?php echo $count; ?> results</p>
                </div>                
            <?php endif; ?>
        </section>

        <section class="search-form-wrapper">
            <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                <label>
                    <input type="search" class="search-field"
                        data-swplive="true"
                        placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>"
                        value="<?php echo get_search_query() ?>" name="s"
                        title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
                </label>
                <input type="submit" class="search-submit"
                    value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
            </form>
        </section>

		<div class="search-results-list">


			<?php if ( have_posts() ): ?>
                <?php while ( have_posts() ): the_post(); ?>

                       <?php get_template_part('template-parts/search/generic-result'); ?>
        

    			<?php endwhile; ?>

            <?php else: ?>

                <article class="search-result-listing no-results">
                    <div class="info">
                        <div class="headline">
                            <h3>No results. Try another search.</h3>
                        </div>
                    </div>
                </article>                

            <?php endif; ?>

			<div class="pagination">
				<?php
					$big = 999999999; // need an unlikely integer
						
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages
					) );
				?>
			</div>
		</div>

	</section>

<?php get_footer(); ?>