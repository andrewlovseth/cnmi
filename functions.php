<?php

/*
	Enqueue Styles & Scripts
*/

function esa_enqueue_child_styles_and_scripts() {

    $dir = get_stylesheet_directory_uri();
    wp_enqueue_style( 'cnmi-styles', $dir . '/cnmi.css', '', false );
    wp_enqueue_script('cnmi-plugins', $dir . '/js/cnmi-plugins.js', array(), false, true );
    wp_enqueue_script('cnmi-scripts', $dir . '/js/cnmi.js', array(), false, true );

}
add_action( 'wp_enqueue_scripts', 'esa_enqueue_child_styles_and_scripts', 99 );


require_once( plugin_dir_path( __FILE__ ) . '/functions/get-sheet-data.php');


function is_blog_page()
{
    global $post;

    // Post type must be 'post'.
    $post_type = get_post_type($post);

    // Check all blog-related conditional tags, as well as the current post type, 
    // to determine if we're viewing a blog page.
    return ( $post_type === 'post' ) && ( is_home() || is_single() );
}