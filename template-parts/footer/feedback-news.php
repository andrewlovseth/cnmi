<?php

    $feedback = get_field('feedback', 'options');
    $feedback_headline = $feedback['headline'];
    $feedback_deck = $feedback['deck'];
    $feedback_cta = $feedback['cta'];

    $news = get_field('news', 'options');
    $news_headline = $news['headline'];
    $news_deck = $news['deck'];
    $news_cta = $news['cta'];

?>


<section class="feedback-and-news grid">
    <div class="feedback">
        <div class="headline">
            <h3><?php echo $feedback_headline; ?></h3>
        </div>

        <div class="copy p2">
            <p><?php echo $feedback_deck; ?></p>
        </div>

        <?php 
            if( $feedback_cta ): 
            $feedback_cta_url = $feedback_cta['url'];
            $feedback_cta_title = $feedback_cta['title'];
            $feedback_cta_target = $feedback_cta['target'] ? $link['target'] : '_self';
        ?>

            <div class="cta">
                <a class="btn orange" href="<?php echo esc_url($feedback_cta_url); ?>" target="<?php echo esc_attr($feedback_cta_target); ?>"><?php echo esc_html($feedback_cta_title); ?></a>
            </div>

        <?php endif; ?>
    </div>

    <div class="news">
        <div class="headline">
            <h3><?php echo $news_headline; ?></h3>
        </div>

        <div class="copy p2">
            <p><?php echo $news_deck; ?></p>
        </div>

        <?php 
            if( $news_cta ): 
            $news_cta_url = $news_cta['url'];
            $news_cta_title = $news_cta['title'];
            $news_cta_target = $news_cta['target'] ? $link['target'] : '_self';
        ?>

            <div class="cta">
                <a class="btn orange" href="<?php echo esc_url($news_cta_url); ?>" target="<?php echo esc_attr($news_cta_target); ?>"><?php echo esc_html($news_cta_title); ?></a>
            </div>

        <?php endif; ?>
    </div>
</section>