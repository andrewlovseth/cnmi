<nav class="site-nav">
	<ul class="main-nav">
		<?php if(have_rows('site_nav', 'options')): while(have_rows('site_nav', 'options')) : the_row(); ?>

			<?php get_template_part('template-parts/header/nav/basic-link'); ?>

			<?php get_template_part('template-parts/header/nav/dropdown'); ?>

		<?php endwhile; endif; ?>

		<?php get_template_part('template-parts/header/get-involved-cta'); ?>

		<?php get_template_part('template-parts/header/search'); ?>
	</ul>
</nav>