<?php if(is_blog_page()) {
	$title = "News";
} elseif(is_category()) {
	$title = 'News: ' . single_cat_title('', false);

} else {
	$title = get_the_title();
}
?>
<?php if( !is_search() && !is_page_template(array('templates/homepage.php', 'templates/project.php'))  ): ?>
	<section class="page-header grid">
		<h1><?php echo $title; ?></h1>
		<h2><?php echo get_field('project_type', 'options'); ?> | <?php echo get_field('project_name', 'options'); ?></h2>
	</section>

	<?php get_template_part('template-parts/header/breadcrumbs'); ?>
<?php endif; ?>