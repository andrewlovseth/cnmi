<?php if( get_row_layout() == 'dropdown' ): ?>

    <li class="nav-item dropdown main-link <?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>">
        <?php 
            $link = get_sub_field('main_link');
            if( $link ): 
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
        ?>

            <a class="top-level-link" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

        <?php endif; ?>

        <ul class="sub-nav">
            <?php if(have_rows('sub_nav')): while(have_rows('sub_nav')): the_row(); ?>
            
            <li class="sub-link">
                <?php 
                    $link = get_sub_field('link');
                    if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>

                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                            <?php if(get_sub_field('icon')): ?>
                                <span class="icon"><img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></span>
                            <?php endif; ?>
                            
                            <span class="label"><?php echo esc_html($link_title); ?></span>
                        </a>

                <?php endif; ?>
            </li>

        <?php endwhile; endif; ?>
        </ul>
    </li>

<?php endif; ?>