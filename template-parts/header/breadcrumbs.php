<?php

global $wp_query;
$object = $wp_query->get_queried_object();
$parent_id  = $object->post_parent;
$depth = 0;
while ($parent_id > 0) {
       $page = get_page($parent_id);
       $parent_id = $page->post_parent;
       $depth++;
}

if($depth == 0) {
    if(is_home()) {
        $second_level_title = "News";
    } else {
        $second_level_title = get_the_title();
    }
}

?>

<nav class="breadcrumbs grid">

    <div class="links">
        <div class="level first-level">
            <a href="<?php echo site_url('/'); ?>">Home</a>
        </div>

        <?php if($depth == 0 && get_post_type( get_the_ID() ) == 'page' || is_home()): ?>

            <div class="level second-level">
                <span class="current"><?php echo $second_level_title; ?></span>
            </div>

        <?php endif; ?>

        <?php if(is_single() && 'post' == get_post_type()): ?>
            <div class="level second-level">
                <a href="<?php echo site_url('/news/'); ?>">News</a>
            </div>

            <div class="level third-level">
                <span class="current"><?php the_title(); ?></span>
            </div>
        <?php endif; ?>

        <?php if(is_category()): ?>
            <div class="level second-level">
                <a href="<?php echo site_url('/news/'); ?>">News</a>
            </div>

            <div class="level third-level">
                <span class="current"><?php single_cat_title(); ?></span>
            </div>
        <?php endif; ?>


    </div>
</nav>