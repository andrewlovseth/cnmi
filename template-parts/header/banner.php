<?php

    $banner = get_field('banner', 'options');
    $show = $banner['show'];
    $copy = $banner['copy'];

    if($show == TRUE):
?>

    <section class="banner grid">
        <div class="copy p2">
            <?php echo $copy; ?>
        </div>
    </section>

<?php endif; ?>