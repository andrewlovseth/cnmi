<?php
 
    if(get_post_type() == 'post' ) {
        $copy = wp_trim_words( get_the_content(), 40, '...' );

    } elseif(get_post_type() == 'page' ) {
        if(get_field('overview_copy')) {
            $copy = wp_trim_words(get_field('overview_copy'), 40, '...' );
        } elseif(get_field('about_site_copy')) {
            $copy = wp_trim_words(get_field('about_site_copy'), 40, '...' );
        } elseif(get_field('page_header_copy')) {
            $copy = wp_trim_words(get_field('page_header_copy'), 40, '...' );
        } elseif(get_field('summary_copy')) {
            $copy = wp_trim_words(get_field('summary_copy'), 40, '...' );
        }
    }

?>

<article class="search-result-listing">
    <div class="info">
        <div class="type">
            <h5><?php echo get_post_type(); ?></h5>
        </div>
        
        <div class="headline">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        </div>

        <?php if($copy): ?>
            <div class="copy p1">
                <?php echo $copy; ?>
            </div>
        <?php endif; ?>
    </div>
</article>