<?php

/*
	Template Name: Review Resources
*/

get_header(); ?>

    <?php get_template_part('templates/review-resources/overview'); ?>

    <?php get_template_part('templates/review-resources/review-processes'); ?>
    
	<?php get_template_part('template-parts/footer/feedback-news'); ?>

<?php get_footer(); ?>