<section class="projects grid">

    <div class="section-header headline">
        <h3><?php echo get_field('projects_headline'); ?></h3>
    </div>

    <?php $projects = get_field('projects'); if( $projects ): ?>
        <div class="projects-grid">
            <?php foreach( $projects as $p ): ?>
                <div class="project" id="<?php echo $p->post_name; ?>">
                    <div class="map">
                        <div class="content">
                            <a href="<?php echo get_permalink( $p->ID ); ?>">
                                <?php $image = get_field('map_image', $p->ID); if( $image ): ?>
                                    <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                                <?php endif; ?>                    
                            </a>
                        </div>
                    </div>

                    <div class="info">
                        <div class="headline">
                            <h4><a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a></h4>
                        </div>

                        <div class="copy p1">
                            <?php echo get_field('short_description', $p->ID); ?>
                        </div>

                        <div class="cta">
                            <a class="btn" href="<?php echo get_permalink( $p->ID ); ?>">Read More</a>
                        </div>
                    </div>
                    
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

</section>