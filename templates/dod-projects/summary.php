<?php

    $summary = get_field('summary');
    $summary_headline = $summary['headline'];
    $summary_copy = $summary['copy'];

    $sidebar = get_field('sidebar');
    $sidebar_headline = $sidebar['headline'];
    $sidebar_copy = $sidebar['copy'];

?>

<section class="summary grid">
    <div class="info">
        <div class="headline">
            <h3><?php echo $summary_headline; ?></h3>
        </div>

        <div class="copy p1 extended">
            <?php echo $summary_copy; ?>
        </div>
    </div>

    <div class="sidebar">
        <div class="headline">
            <h3><?php echo $sidebar_headline; ?></h3>
        </div>

        <div class="copy p3 extended">
            <?php echo $sidebar_copy; ?>
        </div>
    </div>
</section>