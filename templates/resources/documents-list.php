<?php 

$spreadsheet_id = get_field('sheet_id');
$tab_id = get_field('tab_id');
$base_url = get_field('document_base_url');
$columns  = get_field('columns');
$cols_count = count($columns);


$transient_name = 'document_list';
if(get_transient($transient_name)) {
    $entries = get_transient($transient_name);
} else {
    $entries = esa_get_sheet_data($transient_name, $spreadsheet_id, DAY_IN_SECONDS, $tab_id);
}

if(!empty($entries)): ?>

	<div class="documents-list">

		<table id="documents-table">
			<thead>
				<tr>
					<th class="category">Category</th>	
					<th class="title">Document Title</th>
					<th class="file">File</th>									
				</tr>
			</thead>
			
			<tbody>
				<?php if(have_rows('documents_categories')): while(have_rows('documents_categories')): the_row(); ?>
				
					<?php
						$name = get_sub_field('name');
						$slug = sanitize_title_with_dashes($name);
					?>
				
					<tr id="<?php echo $slug; ?>" class="header-row">
						<td class="cat-header" colspan="<?php echo $cols_count; ?>"><?php echo $name; ?></td>				        
					</tr>
			
					<?php
						$files = array_filter($entries, function ($entry) use ($name) {
							return ($entry[0] == $name); }
						);
					
						foreach ($files as $file): ?>

						<?php
							$category = $file[0];
							$title = $file[1];
							$link = $file[2];
							$groupURLs = explode(",", $file[3]);
							$groupLabels = explode(",", $file[4]);
							$groupLinks = array_map(function ($url, $label) {
								return compact('url', 'label');
							}, $groupURLs, $groupLabels);
?>

						<?php if($file[3]): ?>
							<tr class="file grouped">
								<td class="category"><?php echo $category; ?></td>
								<td class="title"><?php echo $title; ?></td>
								<td class="file">
									<?php foreach($groupLinks as $groupLink): ?>
										<a href="<?php echo $groupLink['url']; ?>" target="window"><?php echo $groupLink['label']; ?></a>
									<?php endforeach; ?>
								
								</td>												
							</tr>
						<?php else: ?>
							<tr class="file">
								<td class="category"><?php echo $category; ?></td>
								<td class="title"><?php echo $title; ?></td>
								<td class="file"><a href="<?php echo $link; ?>" target="window">File</a></td>												
							</tr>
						<?php endif; ?>

					<?php endforeach; ?>

				<?php endwhile; endif; ?>

			</tbody>
		</table>
		
	</div>

<?php endif; ?>