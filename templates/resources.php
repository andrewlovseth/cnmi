<?php

/*
	Template Name: Resources
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/content-header'); ?>

	<?php get_template_part('templates/resources/filter'); ?>

	<?php get_template_part('templates/resources/documents'); ?>

<?php get_footer(); ?>