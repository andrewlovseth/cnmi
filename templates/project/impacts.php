<?php

    $impacts = get_field('impacts');
    $section_header = $impacts['section_header'];

    if(have_rows('impacts')): while(have_rows('impacts')): the_row();

?>

<section class="impacts grid">
    <div class="section-header">
        <h3><?php echo $section_header; ?></h3>
    </div>

    <div class="impacts-grid">

        <?php if(have_rows('section')): while(have_rows('section')): the_row(); ?>
    
            <div class="impact">
                <div class="headline">
                    <h4><?php echo get_sub_field('header'); ?></h4>
                </div>

                <div class="copy p2 extended">
                    <?php echo get_sub_field('copy'); ?>
                </div>                
            </div>

        <?php endwhile; endif; ?>

    </div>


</section>

<?php endwhile; endif; ?>