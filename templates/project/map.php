<?php

    $map = get_field('map');
    $image = $map['image'];
    $caption = $map['caption'];

?>

<section class="map grid">

	<div class="photo">
		<div class="content">
            <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
		</div>
	</div>
	
    <div class="caption">
        <div class="caption-wrapper copy p2">
            <?php echo $caption; ?>
        </div>
    </div>		
</section>