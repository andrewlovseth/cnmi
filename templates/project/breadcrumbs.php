<nav class="breadcrumbs grid">

    <div class="links">
        <div class="level first-level">
            <a href="<?php echo site_url('/'); ?>">Home</a>
        </div>

        <div class="level second-level">
            <a href="<?php echo site_url('/dod-activities/'); ?>">DoD Activities</a>
        </div>

        <div class="level third-level">
            <span class="current"><?php the_title(); ?></span>
        </div>
    </div>
</nav>