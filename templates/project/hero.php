<?php

    $hero = get_field('hero');
    $image = $hero['photo'];
    $long_title = $hero['long_title'];

?>

<section class="hero grid">

	<div class="photo">
		<div class="content">
            <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
		</div>
	</div>
	
	<div class="hero-info">
		<div class="headline">
			<h1 class="h1"><?php the_title(); ?></h1>
			<h2 class="h5"><?php echo $long_title; ?></h2>
        </div>		
	</div>

</section>