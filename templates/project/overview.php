<?php

    $overview = get_field('overview');
    $overview_headline = $overview['headline'];
    $overview_copy = $overview['copy'];

    $sidebar = get_field('sidebar');
    $sidebar_headline = $sidebar['headline'];

?>

<section class="overview grid">
    <div class="info">
        <div class="headline">
            <h3><?php echo $overview_headline; ?></h3>
        </div>

        <div class="copy p1 extended">
            <?php echo $overview_copy; ?>
        </div>
    </div>


    <?php if(have_rows('sidebar')): while(have_rows('sidebar')): the_row(); ?>
   
        <div class="sidebar">
            <div class="headline">
                <h3><?php echo $sidebar_headline; ?></h3>
            </div>

            <div class="copy p3 extended">
                <?php if(have_rows('links')): ?>
                    <ul>
                        <?php while(have_rows('links')): the_row(); ?>
                            <li>
                                <?php 
                                    $link = get_sub_field('link');
                                    if( $link ): 
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>

                                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

                                <?php endif; ?>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>

    <?php endwhile; endif; ?>


</section>