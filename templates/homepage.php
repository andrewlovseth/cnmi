<?php

/*
	Template Name: Home
*/

get_header(); ?>

	<?php get_template_part('templates/homepage/hero'); ?>

	<?php get_template_part('templates/homepage/about-site'); ?>

    <?php get_template_part('templates/homepage/timeline'); ?>

	<?php get_template_part('templates/homepage/atlas'); ?>

	<?php get_template_part('templates/homepage/news'); ?>

	<?php get_template_part('template-parts/footer/feedback-news'); ?>
    
<?php get_footer(); ?>