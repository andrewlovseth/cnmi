<?php

/*
	Template Name: Project
*/

get_header(); ?>

	<?php get_template_part('templates/project/hero'); ?>

	<?php get_template_part('templates/project/breadcrumbs'); ?>

	<?php get_template_part('templates/project/overview'); ?>
	
	<?php get_template_part('templates/project/map'); ?>

	<?php get_template_part('templates/project/impacts'); ?>

    <?php get_template_part('templates/homepage/timeline'); ?>

	<?php get_template_part('template-parts/footer/feedback-news'); ?>
    
<?php get_footer(); ?>