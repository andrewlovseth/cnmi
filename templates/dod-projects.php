<?php

/*
	Template Name: DoD Projects
*/

get_header(); ?>

    <?php get_template_part('templates/dod-projects/summary'); ?>

    <?php get_template_part('templates/dod-projects/projects'); ?>

	<?php get_template_part('template-parts/footer/feedback-news'); ?>
    
<?php get_footer(); ?>