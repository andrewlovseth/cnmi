<?php

    $overview = get_field('overview');
    $overview_headline = $overview['headline'];
    $overview_copy = $overview['copy'];

    $sidebar = get_field('sidebar');
    $sidebar_headline = $sidebar['headline'];

?>

<section class="overview grid">
    <div class="info">
        <div class="headline">
            <h3><?php echo $overview_headline; ?></h3>
        </div>

        <div class="copy p1 extended">
            <?php echo $overview_copy; ?>
        </div>
    </div>
   
    <div class="sidebar">
        <div class="headline">
            <h3><?php echo $sidebar_headline; ?></h3>
        </div>

        <div class="copy p3 extended">
            <?php if(have_rows('review_processes')): ?>
                <ul>
                    <?php while(have_rows('review_processes')): the_row(); ?>
                        <li>
                            <?php 
                                $title = get_sub_field('title');
                                $link = sanitize_title_with_dashes($title);
                            ?>

                            <a href="#<?php echo $link; ?>" class="smooth"><?php echo $title; ?></a>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</section>