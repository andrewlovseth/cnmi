<?php if(have_rows('review_processes')): ?>

    <section class="review-processes grid">
        <div class="process-grid">
            <?php while(have_rows('review_processes')): the_row(); ?>
                <?php 
                    $title = get_sub_field('title');
                    $slug = sanitize_title_with_dashes($title);
                    $copy = get_sub_field('summary');
                ?>
                <div class="process" id="<?php echo $slug; ?>">
                    <div class="headline">
                        <h4><?php echo $title; ?></h4>
                    </div>

                    <div class="copy p2 extended">
                        <?php echo $copy; ?>
                    </div>
                    
                </div>
            <?php endwhile; ?>
        </div>
    </section>

<?php endif; ?>