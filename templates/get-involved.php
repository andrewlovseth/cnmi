<?php

/*
	Template Name: Get Involved
*/

get_header(); ?>

	<?php get_template_part('template-parts/global/content-header'); ?>

	<section class="get-involved-content grid">

		<?php get_template_part('templates/get-involved/events'); ?>

		<aside class="sidebar">
			<?php get_template_part('templates/news/subscribe'); ?>

			<?php get_template_part('templates/get-involved/contact-us'); ?>
		</aside>

	</section>

<?php get_footer(); ?>