<?php

$args = array(
    'post_type' => 'post',
    'posts_per_page' => 3
);
$query = new WP_Query( $args );

if ( $query->have_posts() ) :

?>

    <section class="news grid">
        <div class="section-header headline">
            <h3>Latest News</h3>
        </div>

        <div class="news-items">
            
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                <article>
                    <div class="photo">
                        <div class="content">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium'); ?></a>
                        </div>
                    </div>

                    <div class="info">

                        <div class="date">
                            <h5><?php the_time('F j, Y'); ?></h5>
                        </div>

                        <div class="title headline">
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        </div>

                        <div class="cta">
                            <a class="underline" href="<?php the_permalink(); ?>">Read more</a>
                        </div>

                    </div>

                </article>
            
            <?php endwhile; ?>

        </div>

</section>

<?php endif; wp_reset_postdata(); ?>