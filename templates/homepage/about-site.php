<?php 

    $about_site = get_field('about_site');
    $headline = $about_site['headline'];
    $copy = $about_site['copy'];

if(have_rows('about_site')): while(have_rows('about_site')): the_row(); ?>
 
    <section class="about-site grid home-outline">
        <div class="headline">
            <h4 class="h5"><?php echo get_field('about_site_headline'); ?></h4>
        </div>

        <div class="copy p1">
            <?php echo get_field('about_site_copy'); ?>
        </div>

        <?php if(have_rows('ctas')): ?>
            <div class="ctas">
                <?php while(have_rows('ctas')): the_row(); ?>
    
                    <?php 
                        $link = get_sub_field('link');
                        if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>

                        <div class="cta">
                            <a class="btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
                        </div>

                    <?php endif; ?>

                <?php endwhile; ?>
            </div> 
        <?php  endif; ?>

    </section>

<?php endwhile; endif; ?>
