<?php 

    $atlas = get_field('atlas');
    $headline = $atlas['headline'];
    $deck = $atlas['deck'];
    $map = $atlas['map'];

if(have_rows('atlas')): while(have_rows('atlas')): the_row(); ?>

    <section class="atlas grid">
        <div class="legend">
            <div class="headline">
                <h3><?php echo $headline; ?></h3>
            </div>

            <div class="copy p2">
                <?php echo $deck; ?>
            </div>

            <div class="entries">
                <?php if(have_rows('legend')): while(have_rows('legend')): the_row(); ?>
                    <?php $project = get_sub_field('project'); if($project): ?>
    
                        <div class="entry">
                            <div class="name">
                                <h4><a href="<?php echo get_permalink($project->ID); ?>"><?php echo get_the_title($project->ID); ?></a></h4>
                            </div>

                            <div class="description p3">
                                <?php echo get_field('short_description', $project->ID); ?>
                            </div>
                            
                            <div class="cta">
                                <a href="<?php echo get_permalink($project->ID); ?>" class="underline">Read more</a>
                            </div>
                            
                        </div>
                    <?php endif; ?>

                <?php endwhile; endif; ?>

            </div>
        </div>

        <div class="map">
            <div class="content">
                <?php echo wp_get_attachment_image($map['ID'], 'full'); ?>
            </div>
        </div>


    </section>

<?php endwhile; endif; ?>