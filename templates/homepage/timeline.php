<?php 
    $frontpage_id = get_option('page_on_front');
    $timeline = get_field('timeline', $frontpage_id);
    $headline = $timeline['headline'];
    $deck = $timeline['deck'];
    $start = $timeline['timeline_start'];
    $end = $timeline['timeline_end'];
    $timespan = $end - $start;

    function get_duplicates ($array) {
        return array_unique( array_diff_assoc( $array, array_unique( $array ) ) );
    }

    $entries = $timeline['entries'];
    $years = [];
    foreach($entries as $entry) {
        $year = $entry['year'];
        array_push($years, $year);
    }
    $duplicates = get_duplicates($years);


if(have_rows('timeline', $frontpage_id)): while(have_rows('timeline', $frontpage_id)): the_row(); ?>

    <section class="timeline grid">
        <div class="section-header">
            <div class="headline">
                <h3><?php echo $headline; ?></h3>
            </div>

            <div class="copy p1">
                <?php echo $deck; ?>
            </div>
        </div>
        
        <div class="timeline-interactive">
            <div class="timeline-range">
                <div class="start"><?php echo $start; ?></div>
                <div class="line">
                    <?php if(have_rows('entries')): $count = 1; while(have_rows('entries')): the_row(); ?>

                        <?php 
                            $year = get_sub_field('year');
                            $years_ago = $end - $year;
                            $timeline_pct = $years_ago / $timespan;
                            $percent = (float)$timeline_pct * 100 . '%';

                            $classList = 'timeline-link';

                            if($count == 1) {
                                $classList .= ' active';
                            }

                            if (in_array($year, $duplicates)) {
                                $classList .= ' duplicate-year';
                            }
                        ?>

                        <div class="<?php echo $classList; ?>" style="right: <?php echo $percent; ?>">
                            <span class="dot"></span>
                            <span class="label"><?php echo get_sub_field('year'); ?></span>
                        </div>

                    <?php $count++; endwhile; endif; ?>
                </div>
                <div class="end"><?php echo $end; ?></div>
            </div>
            
            <div class="timeline-slider">
                <?php if(have_rows('entries')): while(have_rows('entries')): the_row(); ?>
    
                    <div class="entry">
                        <div class="entry-wrapper">
                            <div class="info">
                                <div class="headline">
                                    <h3><?php echo get_sub_field('year'); ?></h3>
                                </div>

                                <div class="copy p2">
                                    <?php echo get_sub_field('copy'); ?>
                                </div>
                            </div>

                            <div class="photo">
                                <?php $image = get_sub_field('photo'); if( $image ): ?>
                                    <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                <?php endwhile; endif; ?>
            </div>
        </div>
    </section>

<?php endwhile; endif; ?>