<?php 

/*

	Single News Post

*/

get_header(); ?>

    <section class="hero">
        <div class="photo">
            <div class="content">
                <?php the_post_thumbnail(); ?>
            </div>
        </div>	
    </section>

    <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

        <article class="post-article grid">
            <section class="article-header">
                <div class="date">
                    <h4 class="h5"><?php the_time('F j, Y'); ?></h4>
                </div>

                <div class="title headline">
                    <h2 class="h2"><?php the_title(); ?></h2>
                </div>

                <div class="meta">
                    <?php if(has_category()): ?>
                        <div class="categories">
                            <div class="headline">
                                <h5 class="h6">Categories</h5>
                            </div>
                            
                            <?php the_category(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </section>

            <section class="article-body copy p1 extended">
                <?php the_content(); ?>
            </section>
            
        </article>
        
    <?php endwhile; endif; ?>

    <?php get_template_part('template-parts/news/recent-posts'); ?>

<?php get_footer(); ?>